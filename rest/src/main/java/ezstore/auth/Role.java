package ezstore.auth;

public enum Role {
    USER,
    ADMIN,
    OWNER
}
